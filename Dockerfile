FROM python:3
EXPOSE 8080

ADD requirements.txt .
RUN pip install -r requirements.txt

ADD main.py .
CMD uvicorn main:app --host 0.0.0.0 --port 8080
