import os

from dotenv import load_dotenv
from fastapi import FastAPI
from loguru import logger
from shepard_client.api import CollectionApi, DataObjectApi
from shepard_client.api_client import ApiClient
from shepard_client.configuration import Configuration

app = FastAPI()

# Load config
load_dotenv()
shepard_host = os.getenv("SHEPARD_HOST")
shepard_api_key = os.getenv("SHEPARD_API_KEY")

logger.info(f"Running against {shepard_host}")

# Set up shepard client
conf = Configuration(host=shepard_host, api_key={"apikey": shepard_api_key})
conf.access_token = None
client = ApiClient(configuration=conf)
c_api = CollectionApi(client)
do_api = DataObjectApi(client)


@app.get("/collection/{collection_id}")
def read_collection(collection_id: int):
    logger.debug(f"Requested collection {collection_id}")
    collection = c_api.get_collection(collection_id)
    data_objects = do_api.get_all_data_objects(collection_id)
    logger.debug(f"Fetched collection {collection_id}")
    export = {
        "name": collection.name,
        "description": collection.description,
        "data_objects": [
            {"name": do.name, "description": do.description} for do in data_objects
        ],
    }
    return export


@app.get("/")
def healty():
    healty = {"healty": True}
    return healty
